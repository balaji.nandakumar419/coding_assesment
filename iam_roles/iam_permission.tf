resource "aws_iam_policy" "iam_lambda" {
  name = "iam_lambda"
  policy = "${file("iam_lambda.json")}"
}


resource "aws_iam_policy" "iam_rds" {
  name = "iam_rds"
  policy = "${file("iam_rds.json")}"
}


resource "aws_iam_policy" "iam_redis" {
  name = "iam_redis"
  policy = "${file("iam_redis.json")}"
}


resource "aws_iam_policy" "iam_s3" {
  name = "iam_s3"
  policy = "${file("iam_s3.json")}"
}

resource "aws_iam_policy" "iam_ecs" {
  name = "iam_ecs"
  policy = "${file("iam_ecs.json")}"
}

resource "aws_iam_policy" "iam_apigw" {
  name = "iam_apigw"
  policy = "${file("iam_api.json")}"
}

resource "aws_iam_role" "iam_for_terraform" {
   name = "terraform_action_iam_role"

   assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": [
          "ec2.amazonaws.com"
        ]
      },
      "Effect": "Allow",
      "Sid": ""
    },
        {
            "Effect": "Allow",
            "Principal": {
                "Federated": "arn:aws:iam::864370184797:oidc-provider/api.bitbucket.org/2.0/workspaces/balaji419/pipelines-config/identity/oidc"
            },
            "Action": "sts:AssumeRoleWithWebIdentity",
            "Condition": {
                "StringEquals": {
                    "api.bitbucket.org/2.0/workspaces/balaji419/pipelines-config/identity/oidc:aud": "ari:cloud:bitbucket::workspace/9d22b4bb-20ff-4cff-87a3-d4d8fd41e8e4"
                }
            }
        }
  ]
}
EOF
}


resource "aws_iam_role_policy_attachment" "ecs_policy" {
  role       = aws_iam_role.iam_for_terraform.name
  policy_arn = aws_iam_policy.iam_ecs.arn
  depends_on = [aws_iam_role.iam_for_terraform]
}


resource "aws_iam_role_policy_attachment" "redis_policy" {
  role       = aws_iam_role.iam_for_terraform.name
  policy_arn = aws_iam_policy.iam_redis.arn
  depends_on = [aws_iam_role.iam_for_terraform]
}

resource "aws_iam_role_policy_attachment" "lambda_policy" {
  role       = aws_iam_role.iam_for_terraform.name
  policy_arn = aws_iam_policy.iam_lambda.arn
  depends_on = [aws_iam_role.iam_for_terraform]
}

resource "aws_iam_role_policy_attachment" "rds_policy" {
  role       = aws_iam_role.iam_for_terraform.name
  policy_arn = aws_iam_policy.iam_rds.arn
  depends_on = [aws_iam_role.iam_for_terraform]
}

resource "aws_iam_role_policy_attachment" "s3_policy" {
  role       = aws_iam_role.iam_for_terraform.name
  policy_arn = aws_iam_policy.iam_s3.arn
  depends_on = [aws_iam_role.iam_for_terraform]
}


resource "aws_iam_role_policy_attachment" "apigw_policy" {
  role       = aws_iam_role.iam_for_terraform.name
  policy_arn = aws_iam_policy.iam_apigw.arn
  depends_on = [aws_iam_role.iam_for_terraform]
}

resource "aws_iam_role_policy_attachment" "vpc_policy" {
  role       = aws_iam_role.iam_for_terraform.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonVPCFullAccess"
  depends_on = [aws_iam_role.iam_for_terraform]
}
