from flask import Flask,request
from flask import render_template
import socket,requests
import random,ast
import os,json
import argparse

app = Flask(__name__)

color_codes = {
    "red": "#e74c3c",
    "green": "#16a085",
    "blue": "#2980b9",
    "blue2": "#30336b",
    "pink": "#be2edd",
    "default-db": "#130f40"
}

SUPPORTED_COLORS = ",".join(color_codes.keys())

# Get color from Environment variable
COLOR_FROM_ENV = os.environ.get('APP_COLOR')
# Generate a random color
COLOR = random.choice(["red", "green", "blue", "blue2", "darkblue", "pink"])


@app.route('/home', methods=['GET', 'POST'])
def home_data():
  if request.method == "POST":
    #data=(request.data).decode('ascii')
    data=request.get_json()
    print(data)
    #return "hello"
    
    #data = request.data
    if data.get("color",""):
      return render_template('hello.html', name=socket.gethostname(), color=color_codes[data.get("color","default-db")])
    else:
      return render_template('hello.html', name=socket.gethostname(), color=default-db)
  elif request.method == "GET":
      return render_template('hello.html', name=socket.gethostname(), color=color_codes["green"])
		
@app.route('/health_check', methods=['GET'])
def main():
  return "App is heathy"

@app.route('/parse_data', methods=['GET', 'POST'])
def parse_data():
  if request.method == "GET":
      backend_url=os.environ["BURL"]
      r = requests.get('http://'+backend_url+ '/get_data')
      data = r.text
      data=json.loads(data)
      print(data)
      return(data["Update"])

if __name__ == "__main__":
  app.run(host="0.0.0.0", port=8080)
