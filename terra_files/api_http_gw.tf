resource "aws_apigatewayv2_vpc_link" "bals-app-integrator" {
  name               = "bals-private-app"
  security_group_ids = [aws_security_group.vpc_link_sec_group.id]
  subnet_ids         = slice(module.bals_vpc.private_subnet_id,0,var.db_subnet)

  tags = var.module_tags
}


resource "aws_apigatewayv2_api" "bals-app" {
  name          = "bals-http-api"
  protocol_type = "HTTP"
}


resource "aws_apigatewayv2_integration" "bals-app-integration" {
  api_id           = aws_apigatewayv2_api.bals-app.id
  description      = "Private Route Load Balancer"
  integration_type = "HTTP_PROXY"
  integration_uri  = aws_lb_listener.frontend_http[0].arn

  integration_method = "ANY"
  connection_type    = "VPC_LINK"
  connection_id      = aws_apigatewayv2_vpc_link.bals-app-integrator.id

}



resource "aws_apigatewayv2_route" "bals-app-route" {
  api_id    = aws_apigatewayv2_api.bals-app.id
  route_key = "ANY /{proxy+}"

  target = "integrations/${aws_apigatewayv2_integration.bals-app-integration.id}"
}

resource "aws_apigatewayv2_stage" "dev_stage" {
  api_id = aws_apigatewayv2_api.bals-app.id
  auto_deploy = true
  name   = "dev_stage"
}

resource "aws_apigatewayv2_stage" "default_stage" {
  api_id = aws_apigatewayv2_api.bals-app.id
  name   = "$default"
  auto_deploy = true
}

