resource "aws_security_group" "ecs_private_sec_group" {
   name        = "ecs_private_sec_group"
   description = "Security group for ecs tasks"
   vpc_id      = module.bals_vpc.vpc_id

   tags = var.module_tags
 }

 resource "aws_security_group" "elb_private_sec_group" {
   name        = "elb_private_sec_group"
   description = "Security group for ecs tasks"
   vpc_id      = module.bals_vpc.vpc_id

   tags = var.module_tags
 }


 resource "aws_security_group" "lambda_private_sec_group" {
   name        = "lambda_private_sec_group"
   description = "Security group for lambda"
   vpc_id      = module.bals_vpc.vpc_id

   tags = var.module_tags
 }



 resource "aws_security_group" "redis_private_sec_group" {
   name        = "redis_private_sec_group"
   description = "Security group for redis"
   vpc_id      = module.bals_vpc.vpc_id

   tags = var.module_tags
 }


 resource "aws_security_group" "vpc_link_sec_group" {
   name        = "vpc_link_sec_group"
   description = "Security group for ecs tasks"
   vpc_id      = module.bals_vpc.vpc_id

   tags = var.module_tags
 }

resource "aws_security_group" "rds_private_sec_group" {
   name        = "rds_priv_sec_group"
   description = "Security group for rds db"
   vpc_id      = module.bals_vpc.vpc_id

   tags = var.module_tags
 }

 resource "aws_security_group_rule" "outb_ecs_http" {
  type              = "egress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.ecs_private_sec_group.id
}

 resource "aws_security_group_rule" "outb_ecs_https" {
  type              = "egress"
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.ecs_private_sec_group.id
}


 resource "aws_security_group_rule" "outb_private_link_http" {
  type              = "egress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.vpc_link_sec_group.id
}


 resource "aws_security_group_rule" "outb_private_link_https" {
  type              = "egress"
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.vpc_link_sec_group.id
}

 resource "aws_security_group_rule" "outb_elb" {
  type              = "egress"
  from_port         = 8080
  to_port           = 8080
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.elb_private_sec_group.id
}


 resource "aws_security_group_rule" "inb_elb" {
  type              = "ingress"
  from_port         = 0
  to_port           = 65535
  protocol          = "tcp"
  source_security_group_id = aws_security_group.vpc_link_sec_group.id
  security_group_id = aws_security_group.elb_private_sec_group.id
}

resource "aws_security_group_rule" "inb_private_link" {
  type              = "ingress"
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.vpc_link_sec_group.id
}

resource "aws_security_group_rule" "inb_ecs" {
  type              = "ingress"
  from_port         = 8080
  to_port           = 8080
  protocol          = "tcp"
  security_group_id = aws_security_group.ecs_private_sec_group.id
  source_security_group_id = aws_security_group.elb_private_sec_group.id
}

 resource "aws_security_group_rule" "inb_elb_open_vpc" {
  type              = "ingress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = [var.cidr_range]
  security_group_id = aws_security_group.elb_private_sec_group.id
}



 resource "aws_security_group_rule" "out_lambda_https" {
  type              = "egress"
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.lambda_private_sec_group.id
}


 resource "aws_security_group_rule" "out_lambda_http" {
  type              = "egress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.lambda_private_sec_group.id
}

 resource "aws_security_group_rule" "in_rds_db_conn" {
  type              = "ingress"
  from_port         = 3306
  to_port           = 3306
  protocol          = "tcp"
  cidr_blocks       = ["10.3.3.0/28","10.3.3.16/28"]
  security_group_id = aws_security_group.lambda_private_sec_group.id
}






 resource "aws_security_group_rule" "in_redis_app_conn" {
  type              = "ingress"
  from_port         = 6379
  to_port           = 6379
  protocol          = "tcp"
  cidr_blocks       = ["10.3.3.0/28","10.3.3.16/28"]
  security_group_id = aws_security_group.redis_private_sec_group.id
}
