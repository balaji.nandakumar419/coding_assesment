variable "region" { type = string }
variable "module_tags" { type = map }
variable "public_subnet_cidr" { type = list }
variable "private_subnet_cidr" { type = list }
variable "availability_zone" { type = list }
variable "cidr_range" { type = string }
variable "assume_role" { type = string }
variable "elb_subnet" { type = number }
variable "db_subnet" { type = number }

#elb
variable "load_balancer_name" { type = string }
variable "load_balancer_is_internal" { type = bool }
variable "idle_timeout" { type = number }
variable "log_bucket_name" { type = string }
variable "log_location_prefix" { type = string }
variable "create_alb" { type= bool }
variable "logging_enabled"  { type = string }
variable "target_groups_count" { type = number }
#variable "target_groups" { type = list }
#variable "http_tcp_listeners" { type = list }
variable "http_tcp_listeners_count" { type = number }


#####

variable "redis_cluster_name"  { type = string }
variable "redis_log_name"  { type = string }
variable "redis_subnet" { type = number }


######lambda
variable "lambda_name"    { type = string }

variable "lambda_s3_bucket"    { type = string }
variable "lambda_runtime"    { type = string }
variable "lambda_role"    { type = string }
variable "lambda_subnet_ids"  { type = list }
variable "environment_variables" { type = map }
variable "lambda_s3_key"  { type = string }
variable "lambda_timeout" { type = number }
variable "lambda_handler"  { type = string }
##tf-alarm

variable "response_time_threshold" {
  type        = string
  default     = "50"
  description = "The average number of milliseconds that requests should complete within."
}

variable "unhealthy_hosts_threshold" {
  type        = string
  default     = "0"
  description = "The number of unhealthy hosts."
}

variable "healthy_hosts_threshold" {
  type        = string
  default     = "0"
  description = "The number of healthy hosts."
}

variable "evaluation_period" {
  type        = string
  default     = "5"
  description = "The evaluation period over which to use when triggering alarms."
}

variable "statistic_period" {
  type        = string
  default     = "60"
  description = "The number of seconds that make each statistic period."
}

variable "actions_alarm" {
  type        = list(string)
  default     = []
  description = "A list of actions to take when alarms are triggered. Will likely be an SNS topic for event distribution."
}

variable "actions_ok" {
  type        = list(string)
  default     = []
  description = "A list of actions to take when alarms are cleared. Will likely be an SNS topic for event distribution."
}


#sns
variable "your_email" {
type =  string
default = ""
}
