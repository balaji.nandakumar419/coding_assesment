resource "aws_route53_zone" "primary" {
  name = "bals_app.com"
  vpc {
  vpc_id = module.bals_vpc.vpc_id
 }
}

resource "aws_route53_record" "db_route_record" {
  zone_id = aws_route53_zone.primary.id
  name    = "pri_db"
  type    = "CNAME"
  ttl     = 300
  records = [element(split(":",aws_db_instance.app_db.endpoint),0)]
}
