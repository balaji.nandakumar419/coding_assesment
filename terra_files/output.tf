output "elb_target_group_arn" {
  value = aws_lb_target_group.app-tg.*.id
}

output "elb_cname" {
  value = aws_lb.click_app[0].dns_name
}

output "elb_arn_suffix" {
  value = aws_lb.click_app[0].arn_suffix
}

output "aws_route53_zone" {
  value = aws_route53_zone.primary.zone_id
}

output "aws_rds_endpoint" {
value = aws_db_instance.app_db.endpoint
}

output "aws_redis_info" {
value = aws_elasticache_cluster.app_cache_cluster.cache_nodes
}

output "sec_value" {
sensitive = true
value = data.aws_secretsmanager_secret_version.redis_creds.secret_string
}
