resource "aws_iam_role" "iam_for_lambda" {
  name = "iam_for_lambda"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_policy" "s3_policy" {
  name        = "s3_policy"
  path        = "/"
  description = "s3 policy for uploading data "

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
                "s3:GetObject",
                "s3:GetObjectAcl",
                "s3:GetObjectVersion",
                "s3:GetObjectVersionAcl",
                "s3:PutObject",
                "s3:PutObjectAcl",
                "s3:PutObjectVersionAcl",
                "s3:ListBucket",
                "s3:DeleteObject"
        ]
        Effect   = "Allow",
         "Sid": "VisualEditor1"
        Resource = [
                "arn:aws:s3:::bals-infra-files*",
                "arn:aws:s3:::bals-infra-files/*"
]
      },
       {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": "ses:SendRawEmail",
            "Resource": "*"
        }
    ]
  })
}


resource "aws_iam_policy_attachment" "infra_app_attach" {
  name       = "Role attachment for lambda role"
  roles      = [aws_iam_role.iam_for_lambda.name]
  policy_arn = aws_iam_policy.s3_policy.arn
}



resource "aws_iam_policy_attachment" "infra_cw_attach" {
  name       = "Role attachment for lambda role"
  roles      = [aws_iam_role.iam_for_lambda.name]
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}


resource "aws_iam_policy_attachment" "infra_ec2_attach" {
  name       = "Role attachment for lambda role"
  roles      = [aws_iam_role.iam_for_lambda.name]
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ReadOnlyAccess"
}

resource "aws_iam_role_policy_attachment" "AWSLambdaVPCAccessExecutionRole" {
    role       = aws_iam_role.iam_for_lambda.name
    policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaVPCAccessExecutionRole"
}
