locals {
db_sub_count = sum([var.elb_subnet,var.db_subnet])
redis_subnet_count = sum([var.elb_subnet,var.db_subnet,var.redis_subnet])
}
	
	
resource "aws_elasticache_cluster" "app_cache_cluster" {
  cluster_id        = "redis-cache"
  engine            = "redis"
  node_type         = "cache.t3.micro"
  num_cache_nodes   = 1
  port              = 6379
  apply_immediately = true
  security_group_ids =  [aws_security_group.redis_private_sec_group.id]
  subnet_group_name = aws_elasticache_subnet_group.redis_subnet_group.name
  log_delivery_configuration {
    destination      = aws_cloudwatch_log_group.redis_cloudwatch_log_group.name
    destination_type = "cloudwatch-logs"
    log_format       = "text"
    log_type         = "slow-log"
  }
  }
  
resource "aws_elasticache_subnet_group" "redis_subnet_group" {
	subnet_ids = slice(module.bals_vpc.private_subnet_id,local.db_sub_count,local.redis_subnet_count)
	name       = "redis-subnet-group"
}

resource "aws_cloudwatch_log_group" "redis_cloudwatch_log_group" {
  name              = var.redis_cluster_name
  retention_in_days = 1
  tags              = var.module_tags
}


resource "aws_elasticache_user" "default" {
  user_id       = "defaultusrid"
  user_name     = "default"
  access_string = "on ~app::* -@all +@read +@hash +@bitmap +@geo -setbit -bitfield -hset -hsetnx -hmset -hincrby -hincrbyfloat -hdel -bitop -geoadd -georadius -georadiusbymember"
  engine        = "REDIS"
  passwords     = ["4eaT5x!5Qv%WH6aE"]
}

resource "aws_elasticache_user" "admin_user" {
  user_id       = "admin007"
  user_name     = "dataadmin"
  access_string = "on ~app::* -@all +@read +@hash +@bitmap +@geo -setbit -bitfield -hset -hsetnx -hmset -hincrby -hincrbyfloat -hdel -bitop -geoadd -georadius -georadiusbymember"
  engine        = "REDIS"
  passwords     = ["4TyVpp&70dz!$H8L"]
}


resource "aws_elasticache_user_group" "power_user" {
  engine        = "REDIS"
  user_group_id = "power-usr-group"
  user_ids      = [aws_elasticache_user.default.user_id]

  lifecycle {
    ignore_changes = [user_ids]
  }
}

resource "aws_elasticache_user_group_association" "example" {
  user_group_id = aws_elasticache_user_group.power_user.user_group_id
  user_id       = aws_elasticache_user.admin_user.user_id
}
