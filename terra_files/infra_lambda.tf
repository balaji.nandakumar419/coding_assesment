locals {
 env_variables = {
region = "us-east-1"
elb_url = "internal-app-elb-1921487633.us-west-2.elb.amazonaws.com"

} 
}

data "aws_s3_bucket_object" "lambda_s3" {
  bucket = var.lambda_s3_bucket
  key    = var.lambda_s3_key
}

resource "aws_lambda_function" "lambda_function" {
   function_name      = var.lambda_name
   description        = "Lambda for collecting EC2 info"
   s3_bucket          = var.lambda_s3_bucket
   s3_key             = var.lambda_s3_key
   s3_object_version  = data.aws_s3_bucket_object.lambda_s3.version_id
   runtime            = var.lambda_runtime
   timeout            = var.lambda_timeout
   memory_size        = 256
   #source_code_hash   = data.aws_s3_bucket_object.lambda_s3.metadata.Hash
   handler            = var.lambda_handler
   role               = aws_iam_role.iam_for_lambda.arn
   tags               = var.module_tags
   vpc_config {
     subnet_ids         = slice(module.bals_vpc.private_subnet_id,0,var.db_subnet)
     security_group_ids = [aws_security_group.lambda_private_sec_group.id]
   }
   dynamic "environment" {
    for_each = [local.env_variables]
    content {
      variables = environment.value
    }
  }
}

resource "aws_cloudwatch_log_group" "lambda_cloudwatch_log_group" {
  name              = "/aws/lambda/${var.lambda_name}"
  retention_in_days = 1
  tags              = var.module_tags
  depends_on        = [aws_lambda_function.lambda_function]
}

resource "aws_cloudwatch_event_rule" "tag_collector_lambda_event" {
    name = "per_2_hour_tag_collecter"
    description = "Invokes lambda per 2 hours"
    schedule_expression = "rate(120 minutes)"
    tags               = var.module_tags
}

resource "aws_cloudwatch_event_target" "tag_collector_lambda_target" {
    rule = "${aws_cloudwatch_event_rule.tag_collector_lambda_event.name}"
    target_id = "cw_tag_trigger"
    arn = aws_lambda_function.lambda_function.arn
}
