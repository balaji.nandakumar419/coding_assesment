import boto3
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication


def send_mail():
    ses_client = boto3.client('ses', region_name="us-west-2")
    body = "Please find the attached CSV file for missig tags"
    subject = "EC2 Tag Missing details"
    COMMASPACE = ", "
    to_email = ["balslearning0511@gmail.com"]
    message = MIMEMultipart()
    message['Subject'] = subject
    message['From'] = "balaji.nandakumar419@gmail.com"
    message['To'] = COMMASPACE.join(to_email)
    part = MIMEText(body, 'html')
    message.attach(part)
    CHARSET = "utf-8"
    fileName = "reports.csv"
    part=MIMEApplication(open(r"/tmp/report.csv","r").read())
    part.add_header('Content-Disposition', 'attachment', filename=fileName)
    message.attach(part)
    destination = { 'ToAddresses' : [message['To']], 'CcAddresses' : [], 'BccAddresses' : []}
    response = ses_client.send_raw_email(Source=message['From'],Destinations=[message['To']],RawMessage={'Data': message.as_string()})
    print("ses response")
#send_mail()
