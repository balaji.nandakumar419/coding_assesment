import json,ast,logging
import boto3,os
import datetime
import urllib3
import send_mail
from botocore.exceptions import ClientError
from datetime import date
import traceback,csv,time
region = 'us-east-1'
tg_exist=[]
tg_nt_exist=[]
instance_info=dict()


def get_req_details(ins):
    req_info=[]
    global instance_info
    id=ins['Instances'][0].get('InstanceId')
    ins_class=ins['Instances'][0].get('InstanceType')
    image_id=ins['Instances'][0].get('ImageId')
    tag_lst=ins['Instances'][0].get('Tags',[])
    ins_name=[tg.get('Value')  for tg in tag_lst if tg.get('Key')=="Name"]
    if not ins_name:
        ins_name=['NA']

    ins_val="%s"%(str([ins_name[0],ins_class,image_id,tag_lst]))
    req_info.append(ins_val.replace('"',''))
    instance_info[id]=req_info


def get_ec2_info(ec2_obj,filter_tag):
    instance_list=list()
    global region,tg_exist,tg_nt_exist
    response = ec2_obj.describe_instances(Filters=[{'Name': 'instance-state-name','Values': ["running","stopped"]}])
    if response['Reservations']:
      for ins in response['Reservations']:
        get_req_details(ins)
        tag_lst=ins['Instances'][0].get('Tags',[])
        if tag_lst:
          for tg in tag_lst:
            if tg.get('Key')=="Service" and tg.get('Value') in  filter_tag:
                tg_exist.append(ins['Instances'][0].get('InstanceId'))
                #print("tag exists")
            else:
                #print (ins['Instances'][0].get('InstanceId'),"NA")
                tg_nt_exist.append(ins['Instances'][0].get('InstanceId'))


      return(tg_exist,tg_nt_exist)
    else:
        print("No instance found for query")

def write_csv(report_ins):
    fields = ["Instance_name","Instance_class","Image_id","Tags"]
    global instance_info
    final_ins_info=list()
    for i in report_ins:
        val=instance_info[i]
        print(type(ast.literal_eval(val[0])))
        final_ins_info.append(ast.literal_eval(val[0]))
    with open('/tmp/report.csv', 'w') as f:
        write = csv.writer(f)
        write.writerow(fields)
        write.writerows(final_ins_info)


def lambda_handler(event,context):
    ec2_obj=boto3.client('ec2',region_name=region)
    elb_url=os.environ.get("elb_url")
    if event.get("trigger") == 'cloudwatch':
         filter_tags=["Data", "Processing", "Web"]
    else:
        http = urllib3.PoolManager()
        r=http.request('GET','%s/get_tag'%(elb_url))
        #r=http.request('GET','http://gettag.bals_app.com/get_tag')
        resp=(r.data).decode("utf-8")
        filter_tag = list(resp)
        print(resp)
        filter_tags=[resp]
    s3_client = boto3.client('s3')
    tg_exist,tg_nt_exist=get_ec2_info(ec2_obj,filter_tags)
    epochtime=time.time()
    todays_date = datetime.datetime.now()
    if tg_nt_exist or tg_exist:
        report_ins=list(set(tg_nt_exist)- set(tg_exist))
        print(report_ins)
        csv_data=write_csv(report_ins)
        dest_path="%d/%d/%d/%d/report.csv"%(todays_date.year,todays_date.month,todays_date.day,todays_date.hour)
    try:
        response = s3_client.upload_file("/tmp/report.csv",'bals-infra-files' ,dest_path )
        if event.get("test","na") != "true":
            send_mail.send_mail()

    except ClientError as e:
        logging.error(e)


#event = { "trigger":"cloudwatch","test":"na"}
#lambda_handler(event,'context')
