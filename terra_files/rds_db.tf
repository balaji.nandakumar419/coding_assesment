locals {
db_subnet_count = sum([var.elb_subnet,var.db_subnet])
}

resource "aws_db_instance" "app_db" {
  allocated_storage    = 10
  engine               = "mysql"
  engine_version       = "5.7"
  instance_class       = "db.t3.micro"
  username             = "app_backend"
  password             = "juVDlwDhR"
  parameter_group_name = "default.mysql5.7"
  skip_final_snapshot  = true
  db_name              = "app_db"
  db_subnet_group_name = aws_db_subnet_group.app_subnet_group.id
  vpc_security_group_ids = [aws_security_group.rds_private_sec_group.id]
  lifecycle {ignore_changes = all }
}


resource "aws_db_subnet_group" "app_subnet_group" {
  name       = "backend_db_subnet"
  subnet_ids = slice(module.bals_vpc.private_subnet_id,var.elb_subnet,local.db_subnet_count) 
  tags = var.module_tags
}
