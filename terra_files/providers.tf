provider "aws" {
  region = "us-west-2"
  assume_role {
    role_arn     = var.assume_role
  }
}
