data "aws_secretsmanager_secret" "secrets" {
  arn = "arn:aws:secretsmanager:us-west-2:864370184797:secret:stage/redis/creds-uwlF8S"
}

data "aws_secretsmanager_secret_version" "redis_creds" {
  secret_id = data.aws_secretsmanager_secret.secrets.id
}
